#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP7.py
#
#  Copyright 2019 mknigge <mknigge@WORKSTATION>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
import sys
from src.TP7   import *
from src.tools import *
import os

# globals

# classes

# functions

# main
def main(args):
    """

    :param args:
    :return:
    """
    # FP2 main
    TP7(
        # path
        path=os.path.dirname(os.path.realpath(__file__))
    )


if __name__ == "__main__":
    #
    sys.exit(main(sys.argv))
