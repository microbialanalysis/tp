#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP4.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
from src.database import *
from src.tools    import *
from Bio         import SeqIO
from joblib      import Parallel, delayed
import re
import multiprocessing
from itertools import combinations


# init
def init(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # create output dir
    dir("{}/output".format(path))
    # create FP2 output dir
    dir("{}/output/TP4".format(path))
    # input fasta
    input_fasta              = "{}/output/TP3/TP3.aln".format(path)
    # gene file
    manual_identifier        = setting("{}/settings.ini".format(path))["global"]["manual_identifier"]
    # database
    db                       = "{}/output/database/TP.db".format(path)
    # parsed alignment file
    parsed_alignment_file    = "{}/output/TP4/TP4.A.aln".format(path)
    # permutation file
    permutation_file         = "{}/output/TP4/TP4.A.perm".format(path)
    # permutation unique file
    permutation_unique_file  = "{}/output/TP4/TP4.B.perm".format(path)
    # unique alignments file
    unique_alignments_file   = "{}/output/TP4/TP4.B.aln".format(path)
    # align start
    align_start              = setting("{}/settings.ini".format(path))["global"]["align_start"]
    # align stop
    align_end                = setting("{}/settings.ini".format(path))["global"]["align_end"]
    # mismatch threshold
    mismatch_threshold       = float(setting("{}/settings.ini".format(path))["global"]["mismatch_threshold"])
    # return
    return {
        "manual_identifier"       : manual_identifier,
        "db"                      : db,
        "input_fasta"             : input_fasta,
        "parsed_alignment_file"   : parsed_alignment_file,
        "permutation_file"        : permutation_file,
        "permutation_unique_file" : permutation_unique_file,
        "unique_alignments_file"  : unique_alignments_file,
        "align_start"             : align_start,
        "align_end"               : align_end,
        "mismatch_threshold"      : mismatch_threshold
    }


# strip alignment
def strip_alignment(pool, output_file, align_start, align_end):
    """

    :param pool:
    :param output_file:
    :param align_start:
    :param align_end:
    :return:
    """
    # open
    output_file = open(output_file, "w+")
    # for record
    for record in pool:
        # sequence
        sequence = str(record.seq)[int(align_start):int(align_end)]
        # header
        header = str(record.id)
        # not start and end with gaps
        if (sequence[0:3] != "---") and (sequence[-3:] != "---"):
            # write
            output_file.write(">" + header + "\n" + sequence + "\n")
    # close
    output_file.close()


# permutation
def permutation(pool, permutation_file, permutation_unique_file, mismatch_threshold):
    """

    :param pool: generator object holding all records
    :param permutation_file: string absolute path to output file
    :param permutation_unique_file: string absolute path to output file
    :param mismatch_threshold: int threshold mismatch percentage
    :return:
    """
    # open
    output_permutation_unique_file = open(permutation_unique_file, "w+")
    #
    output_permutation_file = open(permutation_file, "w+")
    # container
    container = set()
    # for combination
    for record_x, record_y in combinations(pool, 2):
        # header x
        header_x   = record_x.id
        # header y
        header_y   = record_y.id
        # sequence x
        sequence_x = record_x.seq
        # sequence y
        sequence_y = record_y.seq
        # sequence x length
        sequence_x_length = len(sequence_x)
        # sequence y length
        sequence_y_length = len(sequence_y)
        # gaps x
        gaps_x = sequence_x.count("-")
        # gaps y
        gaps_y = sequence_y.count("-")
        # mismatch count
        mismatch_count = float(score(sequence_x, sequence_y))
        # mismatch percentage
        mismatch_percentage = float(mismatch_count/(sequence_x_length-gaps_x))*100
        # below threshold
        if mismatch_percentage < mismatch_threshold:
            # write
            output_permutation_file.write("{}\n".format(header_y))
            # check set
            if header_y not in container:
                # add
                container.add(header_y)
                # write
                output_permutation_unique_file.write("{}\n".format(header_y))
    # close
    output_permutation_unique_file.close()
    # close
    output_permutation_file.close()
    # return
    return container


# remove duplicates
def remove_duplicates(pool, container, output_file):
    """

    :param pool: Bio object container record
    :param container: set containing unique headers
    :param output_file: string absolute path to output file
    :return:
    """
    # open
    output_file = open(output_file, "w+")
    # for record
    for record in pool:
        # header
        header = record.id
        # sequence
        sequence = record.seq
        # not duplicate
        if header not in container:
            # write
            output_file.write(">{}\n{}\n".format(header, sequence))
    # close
    output_file.close()


# FP4 main
def TP4(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # vars
    vars = init(path = path)
    # database
    db = Database(vars["db"])
    # connect
    db.connect()
    # pool
    pool = SeqIO.parse(vars["input_fasta"], "fasta")
    # strip
    strip_alignment(
        # pool
        pool=pool,
        # output file
        output_file=vars["parsed_alignment_file"],
        # align start
        align_start=vars["align_start"],
        # align end
        align_end=vars["align_end"]
    )
    # pool
    pool = SeqIO.parse(vars["parsed_alignment_file"], "fasta")
    # permutation
    container = permutation(
        # pool
        pool=pool,
        # output permutation file
        permutation_file=vars["permutation_file"],
        # output unique permutation
        permutation_unique_file=vars["permutation_unique_file"],
        # mismatch_threshold
        mismatch_threshold=vars["mismatch_threshold"]
    )
    # pool
    pool = SeqIO.parse(vars["parsed_alignment_file"], "fasta")
    # remove duplicates
    remove_duplicates(
        # records
        pool=pool,
        # container
        container=container,
        # output file
        output_file=vars["unique_alignments_file"]
    )


# main
def main(args):

    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))

