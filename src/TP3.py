#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP3.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
from src.database import *
from src.tools    import *


# init
def init(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # create output dir
    dir("{}/output".format(path))
    # create FP2 output dir
    dir("{}/output/TP3".format(path))
    # output fasta
    output_fasta        = "{}/output/TP3/TP3.fasta".format(path)
    # gene file
    manual_identifier   = setting("{}/settings.ini".format(path))["global"]["manual_identifier"]
    # gene file
    sequence_length     = setting("{}/settings.ini".format(path))["global"]["sequence_length"]
    # mafft
    mafft_exe           = "{}\mafft\mafft".format(path)
    # mafft input file
    mafft_input         = output_fasta
    # mafft output file
    mafft_output        = "{}/output/TP3/TP3.aln".format(path)
    # database
    db                  = "{}/output/database/TP.db".format(path)
    # return
    return {
        "manual_identifier"   : manual_identifier,
        "db"                  : db,
        "sequence_length"     : sequence_length,
        "output_fasta"        : output_fasta,
        "mafft_exe"           : mafft_exe,
        "mafft_input"         : mafft_input,
        "mafft_output"        : mafft_output
    }


# FP3 main
def TP3(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # init
    vars = init(path=path)
    # database
    db = Database(vars["db"])
    # connect
    db.connect()
    # fetch
    df = db.fetch(
        # table
        table="SILVA",
        # select
        select=["ACCESSION", "SEQUENCE", "SEQUENCE_LENGTH", "ORGANISM", "SILVA", "IDENTIFIER"],
        # where
        where={
            "IDENTIFIER": "= \"{}\"".format(vars["manual_identifier"]),
            "SEQUENCE_LENGTH": "> {}".format(vars["sequence_length"])
        }
    )
    # close
    db.close()
    # output fasta
    output_file = open(vars["output_fasta"], "w+")
    # for row
    for row in df:
        # line
        line = (">" + str(row[0]) + "|" + generate_identifier(10) + "|" + str(row[3]) + "|" + str(row[4]) + "|" + vars["manual_identifier"] + "|" + str(
            row[2]) + "|" + "\n" + str(row[1]) + "\n")
        # write
        output_file.write(line)
    # close
    output_file.close()
    # mafft
    mafft(
        # mafft executable
        mafft_exe=vars["mafft_exe"],
        # mafft input
        mafft_input=vars["mafft_input"],
        # mafft output
        mafft_output=vars["mafft_output"]
    )



# main
def main(args):

    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))

