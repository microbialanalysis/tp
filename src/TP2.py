#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP_2.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
from src.database import *
from src.tools    import *
from Bio         import SeqIO
from Bio.Seq     import Seq


# globals
structure = {
    "TABLE_NAME"                    : "SILVA",
    "TIMESTAMP"                     : "TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
    "ACCESSION"                     : "LONGTEXT",
    "SEQUENCE"                      : "LONGTEXT",
    "SEQUENCE_LENGTH"               : "LONGTEXT",
    "ORGANISM"                      : "LONGTEXT",
    "SILVA"                         : "LONGTEXT",
    "IDENTIFIER"                    : "LONGTEXT"
}


# init
def init(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # create output dir
    dir("{}/output".format(path))
    # create FP2 output dir
    dir("{}/output/TP2".format(path))
    # create database
    dir("{}/output/database".format(path))
    # genbank file
    fasta_file = "{}/data/{}".format(path, setting("{}/settings.ini".format(path))["global"]["fasta_file"])
    # gene file
    manual_identifier = setting("{}/settings.ini".format(path))["global"]["manual_identifier"]
    # database
    db = "{}/output/database/TP.db".format(path)
    # return
    return {
        "fasta_file" : fasta_file,
        "manual_identifier"  : manual_identifier,
        "db"                 : db
    }


# parse fasta
def fasta(vars, db):
    """

    :param file:
    :return:
    """
    # collection
    collection = []
    # pool
    pool = SeqIO.parse(vars["fasta_file"], "fasta")
    # for
    for p in pool:
        # accession
        accession       = p.id.split("|")[1]
        # sequence
        sequence        = str(p.seq)
        # sequence length
        sequence_length = len(sequence)
        # organism
        organism        = p.id.split("|")[0]
        # silva
        silva           = p.id.split("|")[2]
        # id
        id              = vars["manual_identifier"]
        # append
        collection.append(
            [
                accession, sequence, sequence_length, organism, silva, id
            ]
        )
    # bulk insert
    db.bulk_insert(
        # table
        table  = "SILVA",
        # values
        values = collection
    )


# FP2 main
def TP2(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # init
    vars = init(path=path)
    # database
    db = Database(vars["db"])
    # connect
    db.connect()
    # create table
    db.create_table(structure)
    # parse fasta
    fasta(vars, db)
    # close
    db.close()



# main
def main(args):

    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))

