#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP4.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
from src.tools    import *
from src.database import *
from Bio         import SeqIO
from Bio.Seq     import Seq
from joblib      import Parallel, delayed
import re
from datetime import datetime
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing

# structure
structure = {
    "TABLE_NAME"            : "",
    "TIMESTAMP"             : "TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
    "ACCESSION"             : "LONGTEXT",
    "SEQUENCE"              : "LONGTEXT",
    "SEQUENCE_LENGTH"       : "LONGTEXT",
    "ORGANISM"              : "LONGTEXT",
    "SILVA"                 : "LONGTEXT",
    "IDENTIFIER"            : "LONGTEXT",
    "SEQUENCE_COUNT"        : "LONGTEXT"
}


# init
def init(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # create output dir
    dir("{}/output".format(path))
    # create FP2 output dir
    dir("{}/output/TP5".format(path))
    # gene file
    manual_identifier      = setting("{}/settings.ini".format(path))["global"]["manual_identifier"]
    # database
    db                     = "{}/output/database/TP.db".format(path)
    # parsed alignment file
    parsed_alignment_file  = "{}/output/TP4/TP4.A.aln".format(path)
    # unique alignments file
    unique_alignments_file = "{}/output/TP4/TP4.B.aln".format(path)
    # return
    return {
        "manual_identifier"      : manual_identifier,
        "db"                     : db,
        "parsed_alignment_file"  : parsed_alignment_file,
        "unique_alignments_file" : unique_alignments_file
    }


# parse fasta
def fasta(record, n):
    """

    :param record: Bio object of record
    :param host: string host for database connection
    :param user: string username for database connection
    :param password: string password for current user for database connection
    :param db: string database name
    :param table: string table name
    :param n: int enumerator
    :return:
    """

    # header
    header          = record.id
    # sequence
    sequence        = str(record.seq)
    # increment
    n               = n + 1
    # accession location
    accession       = header.split("|")[0]
    # sequence length
    sequence_length = len(sequence)
    # organism
    organism        = header.split("|")[2]
    # silva
    silva           = header.split("|")[3]
    # identifier
    identifier      = header.split("|")[4]
    # return
    return(
        [accession, sequence, sequence_length, organism, silva, identifier, n]
    )


# FP5 main
def TP5(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # init
    vars = init(path=path)
    # database
    db = Database(vars["db"])
    # connect
    db.connect()
    # table name
    structure["TABLE_NAME"] = "SILVA_ALIGNED"
    # create table
    db.create_table(structure)
    # table name
    structure["TABLE_NAME"] = "SILVA_ALIGNED_UNIQUE"
    # create table
    db.create_table(structure)
    # pool genbank aligned
    pool_x = SeqIO.parse(vars["parsed_alignment_file"], "fasta")
    # multi
    element_run_x = Parallel(n_jobs=-1)(
        delayed(fasta)(rec, n=i) for i, rec in
        enumerate(pool_x))
    # bulk insert
    db.bulk_insert(
        # table
        table="SILVA_ALIGNED",
        # values
        values=element_run_x
    )
    # pool genbank aligned
    pool_y = SeqIO.parse(vars["unique_alignments_file"], "fasta")
    # multi
    element_run_y = Parallel(n_jobs=-1)(
        delayed(fasta)(rec, n=i) for i, rec in
        enumerate(pool_y))
    # bulk insert
    db.bulk_insert(
        # table
        table="SILVA_ALIGNED_UNIQUE",
        # values
        values=element_run_y
    )
    # close
    db.close()


# main
def main(args):

    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))

