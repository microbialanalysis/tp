#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
import  sqlite3
from    sqlite3 import Error
import  sys, os
import  numpy as np


# database object
class Database():
    """
    """

    def __init__(self, file):
        """

        :param file: string absolute path to database file
        :return:
        """
        # file
        self.file = file
        # connection
        self.connection = None

    # connect
    def connect(self):
        """
        create a database connection to the SQLite database
            specified by self.file
        :param self.file: database file
        :return: Connection object or None
        """
        # try
        try:
            # connection
            self.connection = sqlite3.connect(self.file, timeout=300.0)
        # except
        except Error as e:
            # print
            print(e)

    # close connection
    def close(self):
        """

        """
        # close
        self.connection.close()

    # status
    def status(self):
        """
        check status of database connection
        :return: boolean
        """
        # condition
        if (self.connection) is not None:
            # return
            return True
        # otherwise
        else:
            # return
            return False

    # execute
    def execute(self, query):
        """

        :param query:
        :return:
        """
        # try
        try:
            # connection
            cursor = self.connection.cursor()
            # execute
            cursor.execute(query)
            # fetch
            fetch = np.array(cursor.fetchall())
            # return
            return fetch
        # except
        except Error as e:
            # print
            print(e)

    # drop table
    def drop_table(self, table):
        """

        :param query:
        :return:
        """
        # try
        try:
            # connection
            cursor = self.connection.cursor()
            # execute
            cursor.execute("DROP TABLE IF EXISTS {};".format(table))
        # except
        except Error as e:
            # print
            print(e)

    # create table
    def create_table(self, query):
        """
        create a table from the query statement
        :param query: a CREATE TABLE statement
        :return:
        """
        # create table query
        query = """
            CREATE TABLE IF NOT EXISTS {} (
                {}
            );
                """.format(
            query["TABLE_NAME"],
            "".join(
                [
                    "{} {}, ".format(key, value)
                    for (key, value) in query.items()
                    if key not in ["TABLE_NAME"]
                ]
            )[:-2]
        )
        # try
        try:
            # connection
            c = self.connection.cursor()
            # execute
            c.execute(query)
        # except
        except Error as e:
            # print
            print(e)

    # insert
    def insert(self, table, values):
        """

        :param table: string table name to insert records in
        :param values: collection of lists/tuples all having an
            equal amount of values as columns present in the table
        :return:
        """
        # cursor
        cursor = self.connection.cursor()
        # get column names
        query = "PRAGMA table_info({});".format(table)
        # execute
        cursor.execute(query)
        # fetch
        fetch = np.array(cursor.fetchall())
        # columns
        columns = [fetch[i][1] for i in range(0, len(fetch)) if fetch[i][1] not in ["ID", "TIMESTAMP"]]
        # query
        query = """
            INSERT INTO {}({})
                VALUES({})
            """.format(
            table,
            ",".join(columns),
            ",".join(["?" for x in columns])
        )
        # execute
        cursor.execute(query, values)
        # commit
        self.connection.commit()
        # return
        return cursor.lastrowid

    # build index
    def index(self, table):
        """

        :param table:
        :return:
        """
        # cursor
        cursor = self.connection.cursor()
        #
        query = "PRAGMA table_info({});".format(table)
        # execute
        cursor.execute(query)
        # fetch
        fetch = np.array(cursor.fetchall())
        # columns
        columns = [fetch[i][1] for i in range(0, len(fetch)) if fetch[i][1] not in ["ID", "TIMESTAMP"]]
        # query
        query = """
        CREATE INDEX \"{}_INDEX\" ON \"{}\" (
            {}
        );
        """.format(
            table, table,
            ", ".join([
                "\"{}\" DESC".format(i) for i in columns
            ])
        )
        # try
        try:
            # execute
            cursor.execute(query)
        # except
        except Error as e:
            # print
            print(e)


    # bulk insert
    def bulk_insert(self, table, values):
        """

        :param table: string table name to insert records in
        :param values: collection of lists/tuples all having an
            equal amount of values as columns present in the table
        :return:
        """

        # cursor
        cursor = self.connection.cursor()
        #
        query = "PRAGMA table_info({});".format(table)
        # execute
        cursor.execute(query)
        # fetch
        fetch = np.array(cursor.fetchall())
        # columns
        columns = [fetch[i][1] for i in range(0, len(fetch)) if fetch[i][1] not in ["ID", "TIMESTAMP"]]
        # query
        query = """
            INSERT OR IGNORE INTO {}({})
                VALUES({})
            """.format(
            table,
            ",".join(columns),
            ",".join(["?" for x in columns])
        )
        # try
        try:
            # execute
            cursor.executemany(query, values)
        # except
        except Error as e:
            # print
            print(e)
        # commit
        self.connection.commit()
        # return
        return cursor.lastrowid

    # delete
    def delete(self, query):
        """
        """
        # connection
        cursor = self.connection.cursor()
        # try
        try:
            # execute
            cursor.execute(query)
        # except
        except Error as e:
            # print
            print(e)
        # commit
        self.connection.commit()

    # select
    def fetch(self, table, select, where=None, orderby=None):
        """

        :param table: string table name to insert records in
        :param select: collection of column names
        :return: collection of lists
        """
        # cursor
        cursor = self.connection.cursor()
        # query
        query = """
            SELECT
                {}
            FROM {}
                """.format(
            ", ".join(select),
            table
        )
        # conditional
        if where is not None:
            # query
            query += """
            WHERE
                {}
                """.format(
                "AND ".join(
                    [
                        "{} {} ".format(key, value)
                        for (key, value) in where.items()
                    ]
                )[:-1]
            )
        # conditional
        if orderby is not None:
            # query
            query += """
            ORDER BY {}
                """.format(
                orderby
            )
        # end
        query += ";"
        # execute
        cursor.execute(query)
        # fetch
        fetch = np.array(cursor.fetchall())
        # return
        return fetch


# main
def main(args):
    # return
    return 0

if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))
