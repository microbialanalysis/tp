#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FP4.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
from src.tools    import *
from src.database import *
from Bio         import SeqIO
from datetime import datetime


# init
def init(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # create output dir
    dir("{}/output".format(path))
    # create FP2 output dir
    dir("{}/output/TP4".format(path))
    # gene file
    manual_identifier                    = setting("{}/settings.ini".format(path))["global"]["manual_identifier"]
    # database
    db                      = "{}/output/database/TP.db".format(path)
    # unique alignments file
    unique_alignments_file  = "{}/output/TP4/TP4.B.aln".format(path)
    # oligo_length
    oligo_length            = setting("{}/settings.ini".format(path))["global"]["oligo_length"]
    # temperature_min
    temperature_min         = setting("{}/settings.ini".format(path))["global"]["temperature_min"]
    # temperature_max
    temperature_max         = setting("{}/settings.ini".format(path))["global"]["temperature_max"]
    # occurrences
    occurrences             = setting("{}/settings.ini".format(path))["global"]["occurrences"]
    # qpcr_product_length_min
    qpcr_product_length_min = setting("{}/settings.ini".format(path))["global"]["qpcr_product_length_min"]
    # qpcr_product_length_max
    qpcr_product_length_max = setting("{}/settings.ini".format(path))["global"]["qpcr_product_length_max"]
    # zout_concentratie
    zout_concentratie       = setting("{}/settings.ini".format(path))["global"]["zout_concentratie"]
    # primer_concentratie
    primer_concentratie     = setting("{}/settings.ini".format(path))["global"]["primer_concentratie"]
    # return
    return {
        "manual_identifier"       : manual_identifier,
        "db"                      : db,
        "unique_alignments_file"  : unique_alignments_file,
        "oligo_length"            : oligo_length,
        "temperature_min"         : temperature_min,
        "temperature_max"         : temperature_max,
        "occurrences"             : occurrences,
        "qpcr_product_length_min" : qpcr_product_length_min,
        "qpcr_product_length_max" : qpcr_product_length_max,
        "zout_concentratie"       : zout_concentratie,
        "primer_concentratie"     : primer_concentratie
    }


# generate oligo
def generate_oligo(vars, db):
    """

    :return:
    """
    # structure
    oligo_structure = {
        "TABLE_NAME": "OLIGO",
        "TIMESTAMP": "TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
        "ACCESSION": "LONGTEXT",
        "ORGANISM": "LONGTEXT",
        "SILVA": "LONGTEXT",
        "IDENTIFIER": "LONGTEXT",
        "POSITION": "LONGTEXT",
        "SEQUENCE": "LONGTEXT",
        "SEQUENCE_COUNT": "LONGTEXT",
        "TEMPERATURE": "LONGTEXT",
        "OLIGO_INFO": "LONGTEXT"
    }
    # create table
    db.create_table(oligo_structure)
    # origin sequence, and sequence count
    origin_sequence, n = origin(vars["unique_alignments_file"])
    # pool
    pool = SeqIO.parse(vars["unique_alignments_file"], "fasta")
    # start time
    time_start = datetime.now()
    # args
    args = [
        (
            x, "OLIGO", vars["oligo_length"].split(","),
            min(vars["oligo_length"].split(",")), float(vars["temperature_min"]), float(vars["temperature_max"]),
            float(vars["occurrences"]), float(vars["zout_concentratie"]), float(vars["primer_concentratie"]),
            vars["manual_identifier"], origin_sequence, n
        )
        for x in pool
    ]
    # multi
    product = multi_processing(oligo, args, multiprocessing.cpu_count())
    # filter
    product = sum(list(filter(None, product)), [])
    # end time
    time_end = datetime.now()
    # bulk insert
    db.bulk_insert(
        # values
        values=product,
        # table
        table="OLIGO"
    )
    # execute
    unwanted_nuc = db.execute(
        """
        SELECT
            SUM(
                LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(SEQUENCE, "A", ""), "C", ""), "T", ""), "G", ""))
            ) AS UNWANTED_NUCLEOTIDES
        FROM
            OLIGO
        ;
        """
    )[0][0]
    # execute
    db.delete(
        """
        DELETE
            FROM
                OLIGO
                    WHERE
                    (
                        LENGTH(REPLACE(REPLACE(REPLACE(REPLACE(SEQUENCE, "A", ""), "C", ""), "T", ""), "G", ""))
                    ) > 0
        ;
        """
    )
    # return
    return unwanted_nuc, (time_end-time_start)


# generate primers
def generate_primers(vars, db):
    """

    :param vars:
    :param db:
    :return:
    """
    # structure
    qpcr_structure = {
        "TABLE_NAME": "QPCR",
        "TIMESTAMP": "TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
        "ACCESSION": "LONGTEXT",
        "ORGANISM": "LONGTEXT",
        "SILVA": "LONGTEXT",
        "IDENTIFIER": "LONGTEXT",
        "QPCR_PRODUCT": "LONGTEXT",
        "QPCR_PRODUCT_LENGTH": "INTEGER",
        "POSITION_FORWARD": "INTEGER",
        "POSITION_REVERSE": "INTEGER",
        "SEQUENCE_FORWARD": "LONGTEXT",
        "SEQUENCE_REVERSE": "LONGTEXT",
        "TEMPERATURE_FORWARD": "LONGTEXT",
        "TEMPERATURE_REVERSE": "LONGTEXT",
        "OCCURRENCES_FORWARD": "INTEGER",
        "OCCURRENCES_REVERSE": "INTEGER",
        "SEQUENCE_LENGTH_FORWARD": "INTEGER",
        "SEQUENCE_LENGTH_REVERSE": "INTEGER"
    }
    # create table
    db.create_table(qpcr_structure)
    # pool
    pool = SeqIO.parse(vars["unique_alignments_file"], "fasta")
    # pool oligo
    pool_oligo = db.fetch(
        # table
        table="OLIGO",
        # select
        select=["ACCESSION", "ORGANISM", "SILVA", "IDENTIFIER", "POSITION", "SEQUENCE", "SEQUENCE_COUNT", "TEMPERATURE", "OLIGO_INFO"],
        # where
        where={
            "IDENTIFIER": "= \"{}\"".format(vars["manual_identifier"])
        }
    )[:, 5]
    # start time
    time_start = datetime.now()
    # args
    args = [
        (
            x, int(vars["qpcr_product_length_min"]), int(vars["qpcr_product_length_max"]),
            float(vars["temperature_min"]), pool_oligo
        )
        for x in pool
    ]
    # multi
    product = multi_processing(qpcr, args, multiprocessing.cpu_count())
    #
    product = sum(product, [])
    # bulk insert
    db.bulk_insert(
        # values
        values=product,
        # table
        table="QPCR"
    )
    # index genbank
    db.index("SILVA")
    # index genbank aligned
    db.index("SILVA_ALIGNED")
    # index genbank aligned unique
    db.index("SILVA_ALIGNED_UNIQUE")
    # index oligo
    db.index("OLIGO")
    # index qpcr
    db.index("QPCR")


# FP6 main
def TP6(path):
    """

    :param path: string absolute path to temp directory
    :return:
    """
    # init
    vars = init(path=path)
    # database
    db = Database(vars["db"])
    # connect
    db.connect()
    # generate oligo's
    unwanted_nuc = generate_oligo(vars, db)
    # generate primers
    generate_primers(vars, db)
    # close
    db.close()


# main
def main(args):

    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))

