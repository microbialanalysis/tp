#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#
#  Copyright 2019 mknigge <knigge@microbialanalysis.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# docstring
__author__     = ["Rob Elzinga", "Matthijs Knigge"]
__copyright__  = "Copyright 2019, Microbial Analysis"
__credits__    = ["Rob Elzinga", "Matthijs Knigge"]
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = ["Rob Elzinga", "Matthijs Knigge"]
__email__      = ["elzinga@microbialanalysis.com", "knigge@microbialanalysis.com"]
__status__     = "Production"

# imports
import sys
import os
from   six.moves import configparser
import time
import random
import string
import re
import numpy as np
from joblib      import Parallel, delayed
import re
from datetime import datetime
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing


# calculate melting point
def calc_temperature(oligo, zout_concentratie, primer_concentratie):
    """

    :param oligo_chunk string sequence
    :param zout_concentratie: int salt concentration
    :param primer_concentratie: int primer concentration
    :return:
    """
    enthalpie = 0
    entropie  = 0
    # oligo
    oligo = oligo.upper()
    # increment
    n = 2
    # salt concentration
    zoutconcentratie = float(zout_concentratie)
    # primer concentration
    primer_conc = float(primer_concentratie)
    # seq start
    oligo_start = str(oligo[0])
    # condition
    if oligo_start == ("A" or "T"):
        # start
        start_enthalpie = 9614
        # start
        start_entropie = 17.14
    else:
        # start
        start_enthalpie = 418
        # start
        start_entropie = -11.70
    # oligo end
    oligo_end = str(oligo[len(oligo) - 1])
    # condition
    if oligo_end == ("A" or "T"):
        # end
        end_enthalpie = 9614
        # end
        end_entropie = 17.14
    else:
        # end
        end_enthalpie = 418
        # end
        end_entropie = -11.70
    # total
    totaal_enthalpie = int(start_enthalpie + end_enthalpie)
    # total
    totaal_entropie = float(start_entropie + end_entropie)
    # for duo's
    for i in range(len(oligo) - 1):
        # duo
        duo = (oligo[i:n])
        # switch
        if duo == "AA":
            enthalpie = -33022
            entropie = -92.80
        # switch
        if duo == "AC":
            enthalpie = -35112
            entropie = -93.63
        # switch
        if duo == "AG":
            enthalpie = -32604
            entropie = -87.78
        # switch
        if duo == "AT":
            enthalpie = -30096
            entropie = -85.27
        # switch
        if duo == "CA":
            enthalpie = -35530
            entropie = -94.89
        # switch
        if duo == "CC":
            enthalpie = -33440
            entropie = -83.18
        # switch
        if duo == "CG":
            enthalpie = -44308
            entropie = -113.70
        # switch
        if duo == "CT":
            enthalpie = -32604
            entropie = -87.78
        # switch
        if duo == "GA":
            enthalpie = -34276
            entropie = -92.80
        # switch
        if duo == "GC":
            enthalpie = -40964
            entropie = -101.99
        # switch
        if duo == "GG":
            enthalpie = -33440
            entropie = -83.18
        # switch
        if duo == "GT":
            enthalpie = -35112
            entropie = -93.63
        # switch
        if duo == "TA":
            enthalpie = -30096
            entropie = -89.03
        # switch
        if duo == "TC":
            enthalpie = -34276
            entropie = -92.80
        # switch
        if duo == "TG":
            enthalpie = -35530
            entropie = -94.89
        # switch
        if duo == "TT":
            enthalpie = -33022
            entropie = -92.80
        # total
        totaal_enthalpie = int(totaal_enthalpie + enthalpie)
        # total
        totaal_entropie = float(totaal_entropie + entropie)
        # increment
        n += 1
    # correction
    entropie_gecorr = (totaal_entropie + (
                0.368 * (int(len(oligo) - 1)) * (np.log(zoutconcentratie)) + (8.314 * (np.log((primer_conc) / 4)))))
    # temperature
    tm = round(((totaal_enthalpie / entropie_gecorr) - 273.15 - 4.1), 1)
    # return
    return tm


# origin
def origin(input_file):
    """

    :param input_file: string absolute path to file
    :return:
    """
    # with open
    with open(input_file, "r") as file:
        # file to string
        file = file.read()
        # remove gaps
        sequence = file.replace("-", "")
        # header tag
        header = re.compile(">")
        # count header tags
        n = len(header.findall(sequence))
        # return
        return sequence, n


# backline
def backline():
    """

    :return:
    """
    print('\r', end = "")


# print
def print_(m):
    """

    :param m:
    :return:
    """
    # just print and flush
    print("    " + m, end = "")
    # go one line back
    backline()
    # sleep
    time.sleep(0.5)


# multi processing
def multi_processing(func, args, workers):
    """

    :param func:
    :param args:
    :param workers:
    :return:
    """
    with ProcessPoolExecutor(workers) as ex:
        res = ex.map(func, *zip(*args))
    return list(res)


# multi threading
def multi_threading(func, args, workers):
    """

    :param func:
    :param args:
    :param workers:
    :return:
    """
    with ThreadPoolExecutor(workers) as ex:
        res = ex.map(func, *zip(*args))
    return list(res)

# mafft
def mafft(mafft_exe, mafft_input, mafft_output):
    """

    :param mafft_exe: string absolute path to mafft executable
    :param mafft_input: string absolute path to input file
    :param mafft_output: string absolute path to output file
    :return:
    """
    # system command
    command = "{} --auto --reorder  {} > {}".format(mafft_exe, mafft_input, mafft_output)
    # execute command
    os.system(command)


# create dir
def dir(name):
    """

    :param name: string name of the folder
    :return: string absolute path to folder
    """
    # try
    try:
        # make dir
        os.makedirs(name)
    # except
    except FileExistsError:
        # directory already exists
        pass


# dna direction
def dna_direction(sequence, direction):
    """

    :param sequence: string dna sequence
    :param direction: string numeric
    :return: sequence in forward or reverse composition
    """
    # if reverse
    if direction != "1":
        # reverse
        sequence = sequence.reverse_complement()
    # else forward
    else:
        sequence = sequence
    # return
    return sequence


# parse condig
def setting(path):
    """

    :return: settings
    """
    # config object
    Config = configparser.ConfigParser()
    # parse
    Config.read(path)
    # return
    return(Config)


# chunker
def n_chunks(collection, size):
    # return
    return (collection[i::size] for i in range(size))


# mismatch
def mismatch(sequence_x, sequence_y):
    """

    :param sequence_x: string DNA sequence
    :param sequence_y: string DNA sequence
    :return: numeric mismatch percentage
    """
    num_mm = 0
    for pos in range(0, (len(sequence_x))):
        if sequence_x[pos] == sequence_y[pos]:
            num_mm += 0
        if sequence_x[pos] != sequence_y[pos]:
            num_mm += 1
        # if sequence1[pos] == sequence2[pos] == '-':
        # a += -2
    return num_mm


# mean
def mean(data):
    """
    Return the sample arithmetic mean of data.
    """
    n = len(data)
    if n < 1:
        raise ValueError('mean requires at least one data point')
    return sum(data)/n # in Python 2 use sum(data)/float(n)


# population standard deviation
def _ss(data):
    """
    Return sum of square deviations of sequence data.
    """
    c = mean(data)
    ss = sum((x-c)**2 for x in data)
    return ss


# sample standard deviation
def stddev(data, ddof=0):
    """
    Calculates the population standard deviation
    by default; specify ddof=1 to compute the sample
    standard deviation.
    """
    n = len(data)
    if n < 2:
        raise ValueError('variance requires at least two data points')
    ss = _ss(data)
    pvar = ss/(n-ddof)
    return pvar**0.5


# score
def score(sequence1, sequence2):
    num_mm = 0
    for pos in range(0, (len(sequence1))):
        if sequence1[pos] == sequence2[pos]:
            num_mm += 0
        if sequence1[pos] != sequence2[pos]:
            num_mm += 1
    return num_mm


# random
def generate_identifier(stringLength=6):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))


# oligo builder
def oligo(record, table, oligo_length,
          oligo_length_min, temperature_min, temperature_max,
          occurrences, zout_concentratie, primer_concentratie,
          gene, origin_sequence, n):
    """

    :param host: string host for database connection
    :param user: string username for database connection
    :param password: string password for current user for database connection
    :param db: string database name
    :param table: string table name
    :param record: Bio object holding records
    :param oligo_length: list collection int oligo length
    :param oligo_length_min: int min present oligo length
    :param temperature_min: melting temperature min
    :param temperature_max: melting temperature max
    :param occurrences: int minimum amount of occurrences in origin sequence
    :param zout_concentratie: int salt concentration
    :param primer_concentratie: int primer concentration
    :param gene: string gene to be extracted from genbank file
    :param origin_sequence: string sequence
    :return:
    """
    # header
    header          = record.id
    # sequence
    sequence        = str(record.seq.upper())
    # increment
    n               = n + 1
    # accession
    accession       = header.split("|")[0]
    # sequence length
    sequence_length = len(sequence)
    # organism
    organism        = header.split("|")[2]
    # silva
    silva           = header.split("|")[3]
    # identifier
    identifier      = header.split("|")[4]
    # multi
    element_run = Parallel(n_jobs=-1)(
        delayed(oligo_node)(int(length),
                            r=(len(sequence) - (int(length) - 1)),
                            sequence=sequence,
                            zout_concentratie=zout_concentratie,
                            primer_concentratie=primer_concentratie,
                            gene=gene, origin_sequence=origin_sequence,
                            occurrences=occurrences,
                            temperature_min=float(temperature_min),
                            temperature_max=float(temperature_max),
                            min_length=int(min(oligo_length)),
                            n=n, accession=accession, organism=organism,
                            silva=silva, identifier=identifier
                            ) for length in oligo_length
    )
    # return
    return(
        sum(element_run, [])
    )


# oligo builder child node
def oligo_node(length, r, sequence, zout_concentratie, primer_concentratie, gene,
               origin_sequence, occurrences,
               temperature_max, temperature_min, min_length, n,
               accession, organism, silva, identifier
               ):
    """
    :param length: list collection int different lengths of oligo's
    :param r: range of oligo's
    :param sequence: string entire sequence
    :param zout_concentratie: int salt concentration
    :param primer_concentratie: int primer concentration
    :param gene: string gene to be extracted from genbank file
    :param host: string host for database connection
    :param user: string username for database connection
    :param password: string password for current user for database connection
    :param db: string database name
    :param table: string table name
    :param origin_sequence: string sequence
    :param occurrences: int
    :param temperature_min: int
    :param temperature_max: int
    :param min_length: int minimum oligo length
    :return:
    """
    # product
    product = list()
    # for every position
    for pos in range(r):
        # chunk
        oligo_long = "".join([x for x in sequence[pos:] if x.isalpha()])[0:length]
        # chunk
        oligo_short = oligo_long[0:length-1]
        # temperature
        oligo_long_temperature = calc_temperature(oligo_long, zout_concentratie, primer_concentratie)
        # temperature
        oligo_short_temperature = calc_temperature(oligo_short, zout_concentratie, primer_concentratie)
        # condition
        if (
                (length == min_length) and \
                (oligo_long_temperature >= temperature_min) and \
                (oligo_long_temperature <= temperature_max) \
                ) or \
                (
                        (length > min_length) and \
                        (oligo_short_temperature < temperature_min) and \
                        (oligo_long_temperature >= temperature_min) and \
                        (oligo_long_temperature <= temperature_max)
                ):
            # pattern
            pattern = re.compile(str(oligo_long).upper())
            #
            origin_sequence = origin_sequence.upper()
            # sequence count
            sequence_count = len(pattern.findall(origin_sequence.upper()))
            #
            count_treshold = float(float(occurrences) * n)
            #
            if sequence_count >= count_treshold:
                # return
                product.append(
                    [
                        str(accession),
                        str(organism),
                        str(silva),
                        str(identifier),
                        str(pos),
                        str(oligo_long.upper()),
                        str(sequence_count),
                        str(oligo_long_temperature),
                        "{}_{}_{}_{}_{}".format(gene, pos, oligo_long, oligo_long_temperature, sequence_count)
                    ]
                )
    # return
    return(
        product
    )


# qpcr
def qpcr(record, qpcr_product_length_min, qpcr_product_length_max, temperature_min, oligo):
    """

    :param host: string host for database connection
    :param user: string username for database connection
    :param password: string password for current user for database connection
    :param db: string database name
    :param table: string table name
    :param record: Bio object holding records
    :param qpcr_product_length_min: int minimum length of pcr product
    :param qpcr_product_length_max: int maximum length of qpcr product
    :param temperature_min: melting temperature min
    :return:
    """
    # header
    header = record.id
    # sequence
    sequence = str(record.seq.upper())
    # accession
    accession = header.split("|")[0]
    # sequence length
    sequence_length = len(sequence)
    # organism
    organism = header.split("|")[2]
    # silva
    silva = header.split("|")[3]
    # identifier
    identifier = header.split("|")[4]
    # args
    args = [
                (
                    x, oligo, qpcr_product_length_min, qpcr_product_length_max,
                    temperature_min, header, accession, organism, silva, identifier, sequence
                )
                for x in oligo
            ]
    #
    records = multi_threading(qpcr_parent, args, multiprocessing.cpu_count())
    #
    records = sum(records, [])
    # return
    return(records)


# qpcr node parent
def qpcr_parent(oligo_x, oligo_y, qpcr_product_length_min, qpcr_product_length_max,
                    temperature_min, header, accession, organism, silva, identifier, sequence):
    """

    :return:
    """
    # x
    x = oligo_x
    #
    collection = list()
    #
    for y in oligo_y:
        #
        try:
            #
            collection.append(
                qpcr_node(
                    per_x=x,
                    per_y=y,
                    qpcr_product_length_min=qpcr_product_length_min,
                    qpcr_product_length_max=qpcr_product_length_max,
                    temperature_min=temperature_min,
                    header=header,
                    accession=accession,
                    organism=organism,
                    silva=silva,
                    identifier=identifier,
                    sequence=sequence
                )
            )
        # except
        except:
            # pass
            pass
    # remove None
    collection = list(filter(None, collection))
    # return
    return(collection)


# qpcr node
def qpcr_node(per_x, per_y, qpcr_product_length_min, qpcr_product_length_max, temperature_min,
              header, accession, organism, silva, identifier, sequence):
    """

    :return:
    """
    # forward
    forward = per_x.split("_")
    # gene forward
    gene_forward = forward[0]
    # position forward
    position_forward = forward[1]
    # sequence forward
    sequence_forward = forward[2]
    # occurrence forward
    occurrence_forward = forward[4]
    # temperature forward
    temperature_forward = forward[3]
    # length forward
    length_forward = len(sequence_forward)

    # reverse
    reverse = per_y.split("_")
    # gene reverse
    gene_reverse = reverse[0]
    # position reverse
    position_reverse = reverse[1]
    # sequence reverse
    sequence_reverse = reverse[2]
    # occurrence reverse
    occurrence_reverse = reverse[4]
    # temperature reverse
    temperature_reverse = reverse[3]
    # length reverse
    length_reverse = len(sequence_reverse)

    # qpcr product
    qpcr_product = "{}_fw_{}_rev_{}_{}".format(gene_forward, position_forward, position_reverse, round(temperature_min))
    # qpcr product length
    qpcr_product_length = int(int(position_reverse) - int(position_forward) + int(length_reverse))
    # qpcr identifier


    # condition
    if (sequence_forward in sequence) and (sequence_reverse in sequence):
        # condition
        if (qpcr_product_length >= int(qpcr_product_length_min)) and (
                qpcr_product_length <= int(qpcr_product_length_max)):
            # return
            return (
                [
                    accession,
                    organism,
                    silva,
                    identifier,
                    qpcr_product,
                    qpcr_product_length,
                    position_forward,
                    position_reverse,
                    sequence_forward,
                    sequence_reverse,
                    temperature_forward,
                    temperature_reverse,
                    occurrence_forward,
                    occurrence_reverse,
                    length_forward,
                    length_reverse
                ]
            )



# main
def main(args):
    # return
    return 0


if __name__ == "__main__":
    # exit
    sys.exit(main(sys.argv))
